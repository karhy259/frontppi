import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(private http: HttpClient) { }

  getProviders(): Observable<any> {
    return this.http.get(`http://localhost:3000/api/providers`);
  }
  editProvider(documentId: any, name: any, lastName: any, email: any, cellphone: any, address: any, productsType: any, companyName: any, companyId: any): Observable<any> {
    return this.http.put<any>(`http://localhost:3000/api/providers/${documentId}`,
      {
        name,
        lastName,
        document,
        email,
        cellphone,
        address,
        productsType,
        companyName,
        companyId
      },
      { headers });
  }
  getProvider(id: any): Observable<any> {
    return this.http.get(`http://localhost:3000/api/providers${id}`);
  }
  saveProviders( name: any, lastName: any, document: any, email: any, cellphone: any, address: any, productsType: any, companyName: any, companyId: any): Observable<any> {
    return this.http.post<any>(`http://localhost:3000/api/providers`,
    {
      name,
      lastName,
      document,
      email,
      cellphone,
      address,
      productsType,
      companyName,
      companyId
    },
    { headers });
  }
  deleteProvider(id: any): Observable<any> {
    return this.http.delete(`http://localhost:3000/api/providers${id}`);
  }
}
