import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts(): Observable<any> {
    return this.http.get(`http://localhost:3000/api/products`);
  }
  editProduct(name: any, description: any, date: any, company: any, provider: any, serial: any): Observable<any> {
    return this.http.put<any>(`http://localhost:3000/api/products/${serial}`,
      {
        name,
        description,
        date,
        company,
        provider,
        serial
      },
      { headers });
  }
  getProduct(id: any): Observable<any> {
    return this.http.get(`http://localhost:3000/api/products${id}`);
  }
  saveProducts( name: any, description: any, date: any, company: any, provider: any, serial: any): Observable<any> {
    return this.http.post<any>(`http://localhost:3000/api/products`,
      {
        name,
        description,
        date,
        company,
        provider,
        serial
      },
      { headers });
  }
  deleteProduct(id: any): Observable<any> {
    return this.http.delete(`http://localhost:3000/api/products${id}`);
  }
}
