import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


// components
import { ProductsComponent } from './components/products/products.component';
// Animations
import  {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// Material - kendo

import { MatInputModule, MatSelectModule, MatExpansionModule, MatDialogModule, MatCardModule} from '@angular/material';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { GridModule } from '@progress/kendo-angular-grid';
import { ProvidersComponent } from './components/providers/providers.component';




@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProvidersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    MatSelectModule,
    MatExpansionModule,
    MatDialogModule,
    MatCardModule,
    InputsModule,
    MatInputModule,
    GridModule,
    FormsModule,
    ReactiveFormsModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
