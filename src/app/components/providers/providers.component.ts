import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProviderService } from '../../services/provider.service';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {
  @Input() providers: any;
  @Output() providersChange: EventEmitter<any> = new EventEmitter();

  constructor(private providerService: ProviderService) { }

  provider: any;

  ngOnInit() {
    if (this.providers) {
      this.provider = this.providers;
    } else {
      this.provider = {
        name: '',
        lastName: '',
        documentId: '',
        email: '',
        cellphone: '',
        address: '',
        productsType: '',
        companyName: '',
        companyId: ''
      };
    }
  }

  Search() {

  }

  valueChange() {
    this.providers = this.provider;
    this.providersChange.emit(this.providers);
  }

  saveProvider() {
    this.providerService.saveProviders(this.provider.name, this.provider.lastName, this.provider.documentId, this.provider.email, this.provider.cellphone, this.provider.address, this.provider.productsType, this.provider.companyName, this.provider.companyId).subscribe(response => {
      console.log(response);
    });
  }

  updateProvider() {
    if (this.provider.documentId !== '') {
      this.providerService.editProvider(this.provider.name, this.provider.lastName, this.provider.documentId, this.provider.email, this.provider.cellphone, this.provider.address, this.provider.productsType, this.provider.companyName, this.provider.companyId).subscribe(response => {
        console.log(response);
      });
    }
  }
  deleteProvider() {
    if (this.provider.documentId !== '') {
    this.providerService.deleteProvider(this.provider.documentId).subscribe(response => {
      console.log(response);
    });
  }
  }

  getProvider() {
    if (this.provider.documentId !== '') {
    this.providerService.getProvider(this.provider.documentId).subscribe(response => { console.log(response); });
  }}

  getProviders() {
    this.providerService.getProviders().subscribe(response => { console.log(response); });
  }


}
