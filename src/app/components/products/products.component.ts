import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from '../../services/product.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @Input() products: any;
  @Output() productsChange: EventEmitter<any> = new EventEmitter();

  constructor(private productService: ProductService) { }
  product: any;

  valueChange() {
    this.products = this.product;
    this.productsChange.emit(this.products);
  }

  ngOnInit() {
    if (this.products) {
      this.product = this.products;
    } else {
      this.product = {
        name: '',
        description: '',
        date: '',
        company: '',
        provider: '',
        serial: ''
      };
    }
  }

  saveProduct() {
    this.productService.saveProducts(this.product.name, this.product.description, this.product.date, this.product.company, this.product.provider, this.product.serial).subscribe(response => {
      console.log(response);
    });
  }

  updateProduct() {
    if (this.product.serial != '') {
      this.productService.editProduct(this.product.name, this.product.description, this.product.date, this.product.company, this.product.provider, this.product.serial).subscribe(response => {
        console.log(response);
      });
    }
  }
}
